<?php

namespace Databases\Migrations;

use Databases\Connection;

final class ProductTable extends Connection
{
    public function product()
    {
        $query = 'CREATE TABLE products (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(50) NOT NULL UNIQUE,
price INT NOT NULL,
description VARCHAR(255) NOT NULL,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP
)';
        $statement = $this->connection->prepare($query);
        $statement->execute();
    }
}