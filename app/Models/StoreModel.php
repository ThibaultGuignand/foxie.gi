<?php

namespace App\Models;

use Databases\Connection;

class StoreModel extends Connection
{
    public function get(int $id): array
    {
        $statement = $this->connection->prepare('SELECT * FROM articles WHERE id = :id');
        $statement->bindValue('id', $id, \PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetch();
    }

    public function addCartToDb(): void
    {
        $statement = $this->connection->prepare('INSERT INTO article_user (user_id, article_id) VALUES (:user_id, :article_id)');
        $statement->bindValue('user_id', $_SESSION['id'], \PDO::PARAM_INT);
        $statement->bindValue('article_id', $_GET['id'], \PDO::PARAM_INT);
        $statement->execute();
    }

    public function getArticlesForUser(): array|bool
    {
        $statement = $this->connection->prepare('SELECT a.name, a.content, a.image, a.price FROM articles as a INNER JOIN article_user as au ON a.ID = au.article_id INNER JOIN users as u ON au.user_id = :user_id');
        $statement->bindValue('user_id', $_SESSION['id'], \PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetchAll();
    }

    public function countArticles(): int
    {
        $query = $this->connection->query('SELECT count(*) as cpt from articles');
        $result = $query->fetch();

        return $result['cpt'];
    }

    public function Pagination(int $firstarticle, int $eachpage): array
    {
        $statement = $this->connection->prepare('SELECT * FROM articles ORDER BY price desc limit :firstarticle , :eachpage');
        $statement->bindValue(':firstarticle', $firstarticle, \PDO::PARAM_INT);
        $statement->bindValue(':eachpage', $eachpage, \PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetchAll();
    }
}
