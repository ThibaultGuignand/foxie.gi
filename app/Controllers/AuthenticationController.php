<?php
namespace App\Controllers;
use App\Models\authentication;

class AuthenticationController
{
    public function registerUser(): void
    {
        if (isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
        } else {
            echo('Une des cases est vide.');
        }
        $task = new authentication();
        $task->createUser($username, $password);
        header('Location:/login');
    }

    public function loginUser(): void
    {
        if (isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $task = new authentication();
            $user = $task->getUserByName($username);
            $id = $user["id"];
            $username = $user["username"];
            $hashed_password = $user["password"];
            if (password_verify($password, $hashed_password)) {
                $_SESSION["loggedin"] = true;
                $_SESSION["id"] = $id;
                $_SESSION["user"] = $username;
                $_SESSION['toggle'] = 'ON';
                header("location: /");
                exit();
            } else {
                header("location: /error");
                exit();
            }
        }
    }

    public function logOut()
    {
        session_destroy();
        $_SESSION['toggle'] = 'off';
        header("location: /");
        exit();
    }
}
