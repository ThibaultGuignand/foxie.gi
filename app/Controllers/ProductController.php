<?php

namespace App\Controllers;

use App\Models\StoreModel;

class ProductController
{
    private $storeModel;

    public function __construct()
    {
        $this->storeModel = new StoreModel();
    }

    public function error(): string
    {
        return require './views/error.html';
    }

    public function show(string $id): int
    {
        $article = $this->storeModel->get($id);
        return require './resources/views/article.php';
    }

    public function all(): int
    {
        $currentPage = $_GET['page'] ?? 1;
        $nbrArticles = $this->storeModel->countArticles();
        $eachPage = 3;
        $pages = ceil($nbrArticles / $eachPage);
        $firstArticle = ($currentPage * $eachPage) - $eachPage;
        $items = $this->storeModel->Pagination($firstArticle, $eachPage);
        return require './resources/views/homepage.php';
    }

    public function cart(): int
    {
        $cart = $this->storeModel->getArticlesForUser();
        return require './resources/views/panier.php';
    }

    public function addToCart(): string
    {
        $_SESSION['cart']++;
        return json_encode(['cart' => $_SESSION['cart']]);
    }
}
